import {get_collection, get_all_docs_from_collection,parse_dict_from_doc} from './files_queries.js'
const FILES = 'files';
const OUTSRC = 'outsourcing';
const REFERRALS = 'referrals';
const PROGRAMS = 'programs';
const REASONS = 'ref_reasons';
const USERS = 'users';
const OBJECT_TYPE = 'object'

let delete_program = async function(program_id) {
  let programs = await get_collection(PROGRAMS);
  let response = await programs.doc(program_id).delete();
  return response;
}

let enable_program = async function(program_id){
  let programs = await get_collection(PROGRAMS)
  let doc = await programs.doc(program_id).update({active: true});
  return true;
}

let disable_program = async function(program_id){
  let programs = await get_collection(PROGRAMS)
  let doc = await programs.doc(program_id).update({active: false});
  return true;
}

let get_all_programs = async function() {
  let programs = await get_all_docs_from_collection(PROGRAMS);
  for (var i = 0; i < programs.length; i++) {
    programs[i]['edit_mode'] = false;
  }
  return programs
}

let set_program_name = async function(id, new_name) {
  let programs = await get_collection(PROGRAMS)
  let doc = await programs.doc(id).update({name: new_name});
  return true;
}

let add_program = async function(name) {
  let id = generate_id(3);
  let programs = await get_collection(PROGRAMS)
  await programs.doc(id).set({name: name, id: id, active: true});
  return id;
}

let add_programs_to_user = async function(user_name,new_programs){
  let tempPrograms = {};
  for(let program in new_programs){
    tempPrograms[program] = {program: true}
  };
  let db = await firebase.firestore();
  db.collection("users").doc(user_name).update({
    programs: tempPrograms,
  })
}
let generate_id = function(length) {
  var digits = '0123456789';
  let OTP = '';
  for (let i = 0; i < length; i++ )
  {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return OTP;
}

export {
  delete_program,
  set_program_name,
  get_all_programs,
  add_program,
  enable_program,
  disable_program,
  add_programs_to_user,
};
