import {get_collection} from './files_queries.js'
import * as firebase from "firebase"

let current_date = function()
{
    let today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //As January is 0.
    var yyyy = today.getFullYear();

    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    return (mm + dd + yyyy);
}
let validate_mail = function(mail)
{
    var re = /\S+@\S+\.\S+/;
    return re.test(mail);
}

let validate_user_type = function(user_type)
{
    let temp = user_type.toLowerCase();
    if (temp !== "admin" || temp !== "manager" || temp !== "instructor") {
        return false;
    }
    return true;
}

let add_user = async function(user_type,user_name,user_mail,user_programs) // generate last_seen as Date.now()
{
    let tempProgram = {};
    for(let program in user_programs){
        tempProgram[program] = true;
    }
    let db = await firebase.firestore();
    let date = current_date();
    await db.collection("users").doc(user_name).set({
        last_seen: date,
        mail: user_mail.toLowerCase(),
        type: user_type.toLowerCase(),
        programs: tempProgram,
    });
}
let get_all_user_programs = async function(user_name)
{
    let db = await firebase.firestore();
    let array = [];
    await db.collection('users').doc(user_name).get().then(doc => {
        let programs = doc.data().programs;
        for(let program in programs){
            array.push(program);
        }
    })
    return array;
}
let get_uid = async function(user_name)
{
    let db = await firebase.firestore();
    let data = await db.collection("users").doc(user_name).get().then(doc => doc.data());
    return data.uid;
}
let delete_user_name = async function (user_name)
{
    let db = await firebase.firestore();
    await db.collection("users").doc(user_name).delete().catch(function(error){
            let console = console.log(error);
        });



}

let set_mail = async function(user_name,new_mail)
{
  let db = await firebase.firestore();
    let user_ref = await db.collection("users").doc(user_name);
  return user_ref.update({
      mail: new_mail
  }).catch(function(){
      alert("שגיאה בעדכון מייל");
  })


}

let set_last_seen = async function (user_name) // assign Date.now to last_seen field
{
    let db = await firebase.firestore();
    let user_ref = await db.collection("users").doc(user_name);
    return user_ref.update({
        last_seen: current_date()
    })
}

let set_user_type = async function (user_name, new_type) // assign Date.now to last_seen field
{
    let db = await firebase.firestore();
    let user_ref = await db.collection("users").doc(user_name);
    return user_ref.update({
        type: new_type
    })
}

let get_user_mail = async function (user_name) {
    let db = await firebase.firestore();
    let user = await db.collection("users").doc(user_name).get();

    return user.data().mail;
}

let get_user_type = async function (user_name) {
    let db = await firebase.firestore();
    let user = await db.collection("users").doc(user_name).get();

    return user.data().type;
}

let get_all_users = async function()
{
  let names = [];
  let coll = await get_collection("users");
  await coll.get().then(snapshot => {
    snapshot.forEach(doc => {
      let temp = doc.data();
      temp['id'] = doc.id;
      temp['edit_mode'] = false;
      names.push(temp);
    });
  });
  return names;
}


export {
  add_user,
  delete_user_name,
  set_mail,
  set_last_seen,
  validate_mail,
  validate_user_type,
  get_user_mail,
  get_user_type,
  get_all_users,
  get_uid,
  get_all_user_programs,
  set_user_type,
};
