const Collections = {
    "files": { // dictionary
        "address": "address", // string
        "children": { // dictionary
            "birth_date": "birth_date", // string
            "description": "description", // string
            "objectives": "objectives", // string
        },
        "father_bdate": "father_bdate", // string
        "father_id": "father_id", // string
        "father_name": "father_name", // string
        "father_occ": "father_occ", // string
        "father_phone": "father_phone", // string
        "mail": "mail", // string
        "mother_bdate": "mother_bdate", // string
        "mother_id": "mother_id", // string
        "mother_name": "mother_name", // string
        "mother_occ": "mother_occ", // string
        "mother_phone": "mother_phone", // string
        "ref": "ref", // int --> const.outsourcing
        "signup_date": "signup_date", // string
    },
    "consts": { // dictionary
        "outsourcing": "outsourcing", // dictionary
        "ref_reasons": "ref_reasons" // dictionary
    },
    "users": { // dictionary
        "last_seen": "last_seen", // string
        "mail": "mail", // string
        "user_type": "user_type", // string
    },
    "referrals": { // dictionary
        "address": "address", // string
        "child_id": "child_id", // string
        "child_name": "child_name", // string
        "date": "date", // string
        "employee_name": "employee_name", // string
        "extras": "extras", // string
        "mother_id": "mother_id", // string
        "mother_status": "mother_status", // string
        "mother_name": "mother_name", // string
        "phone": "phone", // string
        "program": "program",// int
        "ref_name": "ref_name", // int const.outsourcing
        "ref_reasons": "ref_reasons", // int array
    },
    "programs": { // dictionary
        "name": "name", // string
        "participants": "participants", // dictionary: key=mom.id, value= int array of child.id's
        "active": "active",
    }

}; 


export {Collections};