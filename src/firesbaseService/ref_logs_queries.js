import {get_collection, get_all_docs_from_collection} from './files_queries.js'
import * as firebase from "firebase"
const FILES = 'files';
const AUTH_REFS = 'auth_referrals';
const REFERRALS_LOG = 'referrals_log';

let delete_referral = async function(ref){
  let db = firebase.firestore();
  await db.collection('referrals_log').doc(ref.ref_id).update({"status": "approved"});
}
let get_all_referrals = async function(){
  let db = await firebase.firestore();
  let refs = [];
  let counter = 0;
  await db.collection("referrals_log").get().then(doc => {
      doc.forEach( ref => {
        refs.push({
          data: ref.data(),
          id: doc.docs[counter].id,
        });
        counter++;
      })

  })
  return refs;
}
let validate_access_code = async function(code) {
  let auth_refs = await get_collection(AUTH_REFS)
  let doc_ref = await auth_refs.doc(code);
  let get_doc = doc_ref.get()
  .then(doc => {
    if (!doc.exists) {
      return false;
    } else {
      return true;
    }
  })
  .catch(() => {
    return false;
  });
  return get_doc;
}


let delete_ref = async function(ref_ac) {
  let auth_refs = await get_collection(AUTH_REFS);
  let response = await auth_refs.doc(ref_ac).delete();
  return response;
}

let get_ref_name_from_access_code = async function(access_code)
{
  if(access_code.length != 6)
  {
    return "";
  }
  let auth_refs = await get_collection(AUTH_REFS);
  let doc = await auth_refs.doc(access_code).get().then(
    a => a.data());
    if (!doc) {
      return "";
    }
    else {
      return doc.name;
    }
  }

  let get_all_refs = async function()
  {
    let refs = await get_all_docs_from_collection(AUTH_REFS);
    for (var i = 0; i < refs.length; i++) {
      refs[i]['edit_mode'] = false;
    }
    return refs
  }
  let get_ref_by_mother_id = async function(mother_id){
    let db = await firebase.firestore();
    let all_refs = [];
    await db.collection("referrals_log").where("mother_id","==",mother_id).get().then(query => {
      query.forEach(doc => {
        all_refs.push(doc.data());
      })
    });
    return all_refs;
  }
  let set_ref_name = async function(access_code, new_name)
  {
    let auth_refs = await get_collection(AUTH_REFS)
    await auth_refs.doc(access_code).update({name: new_name});
    return true;
  }

  let add_ref = async function(name)
  {
    let access_code = generate_access_code(6);
    let auth_refs = await get_collection(AUTH_REFS)
    await auth_refs.doc(access_code).set({name: name, access_code: access_code});
    return access_code;
  }


  let generate_access_code = function(length)
  {
    var digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < length; i++ )
    {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

  async function ref_to_code(ref_name)
  {
    let authed = await get_all_docs_from_collection(AUTH_REFS)
    for(var i=0; i<authed.length; i++){
      if (authed[i].name == ref_name){
        return authed[i].access_code
      }
    }
    return null
  }

  function get_current_datetime() {
    let today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //As January is 0.
    var yyyy = today.getFullYear();

    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;

    var hh = String(today.getHours());
    var mn = String(today.getMinutes());
    var ss = String(today.getSeconds());
    if(mn.length == "1"){
      mn = "0" + mn;
    }
    if(ss.length == "1"){
      ss = "0" + ss;
    }
    if(hh.length == "1"){
      hh = "0" + hh;
    }

    return (dd + mm + yyyy + "_" + hh + mn + ss);
  }

  async function get_refs_by_program(program_id)
  {
    let res = []
    let reflog = await get_all_docs_from_collection(REFERRALS_LOG);
    for (var i = 0; i < reflog.length; i++) {
      if(reflog[i]['doc_id'].startsWith(program_id)){
        res.push(reflog[i])
      }
    }
    return res;
  }

  async function add_log_request(req)
  {
    let date = get_current_datetime();
    let ref_log = await get_collection(REFERRALS_LOG);
    let ref_code = await ref_to_code(req.ref_name);
    ref_code += "_" + date;
    req['ref_id'] = ref_code;
    req['children'] = {};
    await ref_log.doc(ref_code).set(req);
  }

  async function change_paid_status(mother_id, second_id, program, status){
    let files = await get_collection(FILES);
    let doc = await files.doc(mother_id).get();
    let path = get_path_to_program(doc, second_id,program,false);
    let temp = {};
    temp[path] = status;
    await doc.update(temp);
    return true;
  }

  async function get_paid_status(mother_id, second_id, program)
  {
    return !program;
 /*    let files = await get_collection(FILES);
    let doc = await files.doc(mother_id).get();
    let path = get_path_to_program(doc, second_id,program,false);
    let res = doc.data();
    return true; */
    // console.log(path);
    // return paid;
  }

  function get_path_to_program(doc, second_id, program, log){
    let path = "";
    let value = "";
    if (log){
      value = ".log";
    }
    else{
      value = ".paid"
    }
    // Mother program log( no second id )
    if (second_id == null)
    {
      path += "mother_programs" + "." + program + value;
    }

    // Father program log( second id is father's )
    else if(doc['father_id'] == second_id)
    {
      path +=   "father_programs" + "." + program + value;
    }

    // Child program log
    else
    {
      path += "children" + "." + second_id + ".programs." + program + value;
    }
    return path;
  }

  async function add_to_actual_log(mother_id, second_id, program, log_item) {
    let files = await get_collection(FILES);
    let docData = {};
    let docUpdate = await files.doc(mother_id);
   await docUpdate.get().then(doc => docData = doc.data());
    let path = get_path_to_program(docData, second_id,program, true);
    let temp = {};
    temp[path] = firebase.firestore.FieldValue.arrayUnion(log_item);
    await docUpdate.update(temp);
  }
async function change_ref_status(ref_id,status) {
  // OUT STATUSES: "pending" || "approved" || "declined" //
  let refLog = await get_collection("referrals_log");
   await refLog.doc(ref_id).update({"status": status});
}


  export {
    delete_ref,
    set_ref_name,
    get_ref_name_from_access_code,
    get_all_refs,
    add_ref,
    validate_access_code,
    get_refs_by_program,
    ref_to_code,
    add_log_request,
    get_current_datetime,
    add_to_actual_log,
    change_paid_status,
    get_paid_status,
    get_all_referrals,
    delete_referral,
    change_ref_status,
    get_ref_by_mother_id
  };
