import {get_collection, get_all_docs_from_collection,parse_dict_from_doc} from './files_queries.js'
const FILES = 'files';
const OUTSRC = 'auth_referrals';
const REFERRALS = 'referrals';
const PROGRAMS = 'programs';
const REASONS = 'ref_reasons';
const USERS = 'users';
const OBJECT_TYPE = 'object'

let validate_access_code = async function(code) {
  let outsrc = await get_collection(OUTSRC)
  let doc_ref = await outsrc.doc(code);
  let get_doc = doc_ref.get()
  .then(doc => {
    if (!doc.exists) {
      console.log('No such access code!');
      return false;
    } else {
      return true;
    }
  })
  .catch(err => {
    console.log('Error getting document', err);
    return false;
  });
  return get_doc;
}


let delete_ref = async function(ref_ac) {
  let outsrc = await get_collection(OUTSRC);
  let response = await outsrc.doc(ref_ac).delete();
  return response;
}

let get_ref_name_from_access_code = async function(access_code)
{
  if(access_code.length != 6)
  {
    return "";
  }
  let outsrc = await get_collection(OUTSRC);
  let doc = await outsrc.doc(access_code).get().then(
    a => a.data());
    if (!doc) {
      return "";
    }
    else {
      return doc.name;
    }
  }

  let get_all_refs = async function() {
    let refs = await get_all_docs_from_collection(OUTSRC);
    for (var i = 0; i < refs.length; i++) {
      refs[i]['edit_mode'] = false;
    }
    return refs
  }

  let set_ref_name = async function(access_code, new_name) {
    let outsrc = await get_collection(OUTSRC)
    let doc = await outsrc.doc(access_code).update({name: new_name});
    return true;
  }

  let add_ref = async function(name) {
    let access_code = generate_access_code(6);
    let outsrc = await get_collection(OUTSRC)
    await outsrc.doc(access_code).set({name: name, access_code: access_code});
    return access_code;
  }


  let generate_access_code = function(length) {
    var digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < length; i++ )
    {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

  export
  {
    delete_ref,
    set_ref_name,
    get_ref_name_from_access_code,
    get_all_refs,
    add_ref,
    validate_access_code,

  }
