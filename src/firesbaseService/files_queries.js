import * as firebase from "firebase";

const FILES = "files";
const OBJECT_TYPE = "object";


async function get_collection(collection_name) {
  let db = await firebase.firestore();
  return await db.collection(collection_name);
}
let get_all_docs_from_collection = async function(collection_name) {
  let names = [];
  let coll = await get_collection(collection_name);
  await coll.get().then(snapshot => {
    snapshot.forEach(doc => {
      names.push(doc.data());
    });
  });
  return names;
};
let add_file = async function(file) {
  let children = file.children;
  let temp = {};
  for (let child in children) {
    let tempPrograms = {};
    for(let program in children[child].programs)
    {
      tempPrograms[program] = {
        paid: false,
        log: children[child].programs[program].log,
      };
  }
    temp[children[child].id] = {
      birth_date: children[child].birth_date ? children[child].birth_date : "",
      description: children[child].description ? children[child].description : "",
      name: children[child].name ? children[child].name : "",
      objectives: children[child].objectives ? children[child].objectives : "",
      programs: tempPrograms
    };
  }
  let momPrograms = file.mother_programs;
  let fixedMomPrograms = {};
  for(let program in momPrograms){
    fixedMomPrograms[program] = {
      paid: false,
      log: momPrograms[program].log ? momPrograms[program].log : []
    };
  }
  let dadPrograms = file.father_programs;
  let fixedDadPrograms = {};
  for(let program in dadPrograms){
    fixedDadPrograms[program] = {
      paid: false,
      log: dadPrograms[program].log ? dadPrograms[program].log : []
    };
  }

  let db = firebase.firestore();
  try {
    await db
      .collection("files")
      .doc(file.mother_id)
      .set({
        address: file.address,
        children: temp,
        father_id: file.father_id,
        father_name: file.father_name,
        father_last_name: file.father_last_name,
        father_full_name: file.father_name + " " + file.father_last_name,
        father_bdate: file.father_bdate,
        father_occ: file.father_occ,
        father_phone: file.father_phone,
        father_programs: fixedDadPrograms,
        mother_id: file.mother_id,
        mother_name: file.mother_name,
        mother_last_name: file.mother_last_name,
        mother_full_name: file.mother_name + " " + file.mother_last_name,
        mother_bdate: file.mother_bdate,
        mother_occ: file.mother_occ,
        mother_phone: file.mother_phone,
        mother_programs: fixedMomPrograms,
        ref: file.ref,
        mail: file.mail,
        signup_date: file.signup_date
      });
  } catch (e) {
    alert("שגיאה בהוספת תיק");
  }
};

let parse_dict_from_doc = function(doc) {
  let doc_obj = {};
  for (var att in doc) {
    if (doc.hasOwnProperty(att)) {
      if (typeof doc[att] == OBJECT_TYPE) {
        doc_obj[att] = parse_dict_from_doc(doc[att]);
      } else {
        doc_obj[att] = doc[att];
      }
    }
  }
  return doc_obj;
};

let get_file = async function(
  mother_id // return a dictionary of the file data.
) {
  let db = await firebase.firestore();
  let file = await db
    .collection("files")
    .doc(mother_id)
    .get()
    .then(doc => doc.data());
  return file;
};

let get_file_by_child_id = async function(
  child_id // return family file according to child id
) {
  let wanted_file = [];
  let family_files = await get_all_docs_from_collection(FILES); // getting all family files
  family_files.forEach(doc => {
    // iterating through each family
    for (var property in doc.children) {
      // for each family iterating through the child object
      if (doc.children.hasOwnProperty(property)) {
        if (property === child_id) {
          // if we have a child with the same child_id that the family we want
          wanted_file.push(doc); // push doc.children[child_id] for the child file only
          break; // breaks from the loop
        }
      }
    }
  });
  return wanted_file;
};

let all_files_by_parent_name = async function(
  parent_name // return file id array with limit_latest size. sorted by date
) {
  
  let files = [];
  let db = firebase.firestore();

  await db
    .collection("files")
    .where("mother_full_name", "==", parent_name)
    .get()
    .then(doc => {
      doc.forEach(element => {
        files.push(element.data());
      });
    });
  if (files[0] === undefined) {
    await db
      .collection("files")
      .where("father_full_name", "==", parent_name)
      .get()
      .then(doc => {
        doc.forEach(element => {
          files.push(element.data());
        });
      });
  }
  return files;
};

let all_files_by_child_name = async function(child_name) {
  let db = firebase.firestore();
  let files = [];
  await db
    .collection("files")
    .get()
    .then(querySnapshot => {
      querySnapshot.forEach(doc => {
          let children = doc.data().children;
          for (var child in children) {
            if (children[child].name === child_name) {
              files.push(doc.data());
            }
          }
          
        })
      });
  return files;
};
let all_files_by_child_birth_date = async function(birth_date) {
  let db = firebase.firestore();
  let files = [];
  await db
    .collection("files")
    .get()
    .then(querySnapshot => {
      querySnapshot.forEach(doc => {
        let file = doc.data().children;
        for (var key in file) {
          if (file[key].birth_date === birth_date) {
            files.push(doc.data());
          }
        }
      });
    });
  return files;
};
let all_files_by_last_name = async function(last_name) {
  let db = firebase.firestore();
  let files = {};
  await db
    .collection("files")
    .where("father_last_name", "==", last_name)
    .get()
    .then(doc => {
      doc.forEach(element => {
        files[element.data().mother_id] = element.data(); 
      });
    });
    await db
      .collection("files")
      .where("mother_last_name", "==", last_name)
      .get()
      .then(doc => {
        doc.forEach(element => {
          if(! files.hasOwnProperty(element.data().mother_id)){
            files[element.data().mother_id] = element.data(); 
          }
        });
      });
  
  return Object.values(files);
};
let get_file_by_father_id = async function(id) {
  let db = firebase.firestore();
  let files = [];
  await db
    .collection("files")
    .where("father_id", "==", id)
    .limit(1)
    .get()
    .then(doc => {
      doc.forEach(element => {
        files.push(element.data());
      });
    });
  return files;
};

export {
  parse_dict_from_doc,
  get_file,
  get_all_docs_from_collection,
  get_collection,
  all_files_by_parent_name,
  all_files_by_child_name,
  add_file,
  all_files_by_child_birth_date,
  all_files_by_last_name,
  get_file_by_father_id,
  get_file_by_child_id,
};
