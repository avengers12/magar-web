import {get_collection, get_all_docs_from_collection} from './files_queries.js'
const AUTH_REFS = 'auth_referrals';
const REFERRALS = 'referrals';

let validate_access_code = async function(code) {
  let auth_refs = await get_collection(AUTH_REFS)
  let doc_ref = await auth_refs.doc(code);
  let get_doc = doc_ref.get()
  .then(doc => {
    if (!doc.exists) {
      return false;
    } else {
      return true;
    }
  })
  .catch( () => {
    return false;
  });
  return get_doc;
}


let delete_ref = async function(ref_ac) {
  let auth_refs = await get_collection(AUTH_REFS);
  let response = await auth_refs.doc(ref_ac).delete();
  return response;
}

let get_ref_name_from_access_code = async function(access_code)
{
  if(access_code.length != 6)
  {
    return "";
  }
  let auth_refs = await get_collection(AUTH_REFS);
  let doc = await auth_refs.doc(access_code).get().then(
    a => a.data());
    if (!doc) {
      return "";
    }
    else {
      return doc.name;
    }
  }

  let get_all_refs = async function() {
    let refs = await get_all_docs_from_collection(AUTH_REFS);
    for (var i = 0; i < refs.length; i++) {
      refs[i]['edit_mode'] = false;
    }
    return refs
  }

  let set_ref_name = async function(access_code, new_name) {
    let auth_refs = await get_collection(AUTH_REFS)
    let doc = await auth_refs.doc(access_code).update({name: new_name});
    return true;
  }

  let add_ref = async function(name) {
    let access_code = generate_access_code(6);
    let auth_refs = await get_collection(AUTH_REFS)
    await auth_refs.doc(access_code).set({name: name, access_code: access_code});
    return access_code;
  }


  let generate_access_code = function(length) {
    var digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < length; i++ )
    {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  }

let get_refs_by_program = async function(program_id){
  let res = []
  let reflog = await get_all_docs_from_collection(REFERRALS);
  for (var i = 0; i < reflog.length; i++) {
    if(reflog[i]['doc_id'].startsWith(program_id)){
      res.push(reflog[i])
    }
  }
  return res;
}

  export
  {
    delete_ref,
    set_ref_name,
    get_ref_name_from_access_code,
    get_all_refs,
    add_ref,
    validate_access_code,
    get_refs_by_program
  }
