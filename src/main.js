import Vue from "vue";
import Vuex from 'vuex'
import Router from "vue-router";
import App from "./App.vue";
import * as firebase from "firebase";
import "firebase/firestore";
import {config} from '../doNotUpload/firebaseConfig'
import ReferralsForm from './components/ReferralsForm'
import SignIn from './components/SignIn'
import 'es6-promise/auto'
import EvaIcons from 'vue-eva-icons'
import AdminPanel from './components/AdminPanel/AdminPanel'
import UsersPanel from './components/AdminPanel/UsersPanel'
import ReferralsPanel from './components/AdminPanel/ReferralsPanel'
import ProgramsPanel from './components/AdminPanel/ProgramsPanel'
import Files from './components/Files/Files.vue'
import AddFile from './components/Files/AddFile.vue'
import Queries from "./components/Queries/Queries.vue";
import Referrals from './components/ReferralsLog/Referrals.vue'
import SingleFileDisplay from './components/Files/SingleFileDisplay.vue'
import * as users_queries from './firesbaseService/users_queries'

Vue.use(EvaIcons);
Vue.use(Vuex)
Vue.use(Router);

Vue.config.productionTip = false;

firebase.initializeApp(config);
const store = new Vuex.Store({
    state: {
        // variables that will be gloabl.
        user_name: "",
        user_mail: "",
        user_type: "",
        loggedIn: false,
        backgroundOpacityIsOn: false,
        dbPrograms: [],
        momPrograms: [],
        dadPrograms: [],
        userPrograms: [],
        allFiles: [],
        wantedFiles: [],
        filesToSave: {},
        secondaryApp: firebase.initializeApp(config, "Secondary")
    },
    mutations: {
        // setters functions to use across any component
        filesToSave(state, files) {
            state.filesToSave = files;
        },
        addFile(state, file) {
            state.wantedFiles.push(file);
        },
        deleteAllWantedFiles(state) {
            state.wantedFiles = [];
        },
        setDbPrograms(state, program) {
            state.dbPrograms.push(program);
        },
        setAllFiles(state, files) {
            state.allFiles = files;
        },
        async set_user_programs(state) {
            await firebase.auth().onAuthStateChanged(async function (user) {
                if (user) {
                    let userName = user.displayName;
                    let programs = await users_queries.get_all_user_programs(userName);
                    programs.forEach(program => {
                        state.userPrograms.push(program);
                    });
                } else {
                    state.userPrograms = [];
                }
            });
        },
        async set_user_name(state) {
            // the state represents all of global variables and the name represents the new name.
            await firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    state.user_name = user.displayName;
                } else {
                    state.user_name = "";
                }
            });
        },
        async set_user_mail(state) {
            await firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    state.user_mail = user.email;
                } else {
                    state.user_mail = "";
                }
            });
        },
        set_user_type(state, type) {
            state.user_type = type;
        },
        async setLogIn(state) {
            await firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    state.loggedIn = true;
                } else {
                    state.loggedIn = false;
                }
            });
        },
        setBackgroundOpacityIsOn(state, type) {
            state.backgroundOpacityIsOn = type;
        }
    },
    getters: {
        user_name: state => state.user_name, // the state is a paramater of the getter function and we return the right var of that state
        user_mail: state => state.user_mail,
        user_type: state => state.user_type,
        isLoggedIn: state => state.loggedIn,
        getBackgroundOpacityIsOn: state => state.backgroundOpacityIsOn,
        getDbPrograms: state => state.dbPrograms,
        getUserPrograms: state => state.userPrograms,
        getAllFiles: state => state.allFiles,
        getWantedFiles: state => state.wantedFiles,
        getFilesToSave: state => state.filesToSave,
        getSecondaryApp: state => state.secondaryApp,
    }
});

const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "SignIn",
            component: SignIn
        },
        {
            path: '/singleFileDisplay',
            name: 'SingleFileDisplay',
            component: SingleFileDisplay,
            props: true,
        },
        {
            path: "/referrals",
            name: "Referrals",
            component: Referrals
        },
        {
            path: "/addFile",
            name: "AddFile",
            component: AddFile,
            props: true
        },
        {
            path: "/files",
            name: "Files",
            component: Files
        },
        {
            path: "/admin",
            component: AdminPanel,
            children: [
                {
                    path: "/",
                    name: "UsersPanel",
                    component: UsersPanel
                },
                {
                    path: "programs",
                    name: "ProgramsPanel",
                    component: ProgramsPanel
                },
                {
                    path: "referrals",
                    name: "ReferralsPanel",
                    component: ReferralsPanel
                }
            ]
        },
        {
            path: "/referralsForm",
            name: "ReferralsForm",
            component: ReferralsForm
        },
        {
            path: "/queries",
            name: "Queries",
            component: Queries
        },
        {
            path: "*",
            redirect: "/"
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.name === 'ReferralsForm') {
        next();
    }
    else {
        firebase.auth().onAuthStateChanged(user => {
            if (!user) {
                store.commit("setLogIn", false);
                if (to.name === "SignIn") {
                    next();
                } else {
                    next('/');
                }
            } else {
                store.commit("setLogIn", true);
                if (to.path === "/admin") {
                    if (store.getters.user_type === "admin") {
                        next();
                    } else {
                        next(false);
                    }
                } else {
                    next();
                }
            }
        });
    }

});

new Vue({
    el: "#app",
    store,
    router,
    firebase,
    template: "<App/>",
    components: {App}
});
